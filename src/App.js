import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Header, Button, Spinner } from './components/common';
import LoginForm from './components/LoginForm';


class App extends Component {
    state = { loggedIn: null }
    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyBAAekhK-dCcWAXxWNL0TqR1eK5_Shvv2w',
            authDomain: 'authentification-fef2d.firebaseapp.com',
            databaseURL: 'https://authentification-fef2d.firebaseio.com',
            projectId: 'authentification-fef2d',
            storageBucket: 'authentification-fef2d.appspot.com',
            messagingSenderId: '113636407723'
        });

        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({ loggedIn: true });
            } else {
                this.setState({ loggedIn: false });
            }
        });
    }

    renderContent() {
        switch (this.state.loggedIn) {
            case true:
                return <Button>Log Out</Button>;
            case false:
                return <LoginForm />;
            default:
                return (
                    <View style={styles.spinnerStyle}>
                        <Spinner size="large" />
                    </View>
                );
        }
        // if (this.state.loggedIn) {
        //     return (
        //         <Button>
        //             Log Out
        //         </Button>
        //     );
        // }

        // return <LoginForm />;
    }

    render() {
        return (
            <View>
                <Header judul="Authentification" />
                <LoginForm />
            </View>
        );
    }
}

const styles = {
    spinnerStyle: {
        flex: 1,
        marginTop: 240,
        justifyContent: 'center',
        alignItems: 'center'
    }
};

export default App;
